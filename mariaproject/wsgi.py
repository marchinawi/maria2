from flask import Flask, render_template
import os

PEOPLE_FOLDER = os.path.join('static', 'people_photo')

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = "static"

@app.route('/')
@app.route('/index')
def show_index():
    full_filename = os.path.join(app.config['UPLOAD_FOLDER'], 'picture2.jpg')
    full_filename2 = os.path.join(app.config['UPLOAD_FOLDER'], 'picture1.jpg')
    full_filename3 = os.path.join(app.config['UPLOAD_FOLDER'], 'picture3.jpg')
    full_filename4 = os.path.join(app.config['UPLOAD_FOLDER'], 'picture4a.jpg')
    full_filename5 = os.path.join(app.config['UPLOAD_FOLDER'], 'picture5.jpg')
    full_filename7 = os.path.join(app.config['UPLOAD_FOLDER'], 'picture4.jpg')
    full_filename8 = os.path.join(app.config['UPLOAD_FOLDER'], 'picture7.jpg')
    full_filename9 = os.path.join(app.config['UPLOAD_FOLDER'], 'picture6.jpg')
    full_filename10 = os.path.join(app.config['UPLOAD_FOLDER'], 'picture9.jpg')
    full_filename11= os.path.join(app.config['UPLOAD_FOLDER'], 'picture10.jpg')
    return render_template("index.html", user_image = full_filename,user_image2 = full_filename2,user_image3 = full_filename3,user_image4 = full_filename4
                           ,user_image5 = full_filename5,user_image7 = full_filename7,user_image8 = full_filename8,user_image9 = full_filename9,
                           user_image10 = full_filename10,user_image11 = full_filename11)


if __name__ == '__main__':
    app.run()