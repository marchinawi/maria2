from flask import Flask, render_template
import os

PEOPLE_FOLDER = os.path.join('static', 'people_photo')

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = "static"

@app.route('/')
@app.route('/index')
def show_index():
    full_filename = os.path.join(app.config['UPLOAD_FOLDER'], 'pic1.jpg')
    full_filename2 = os.path.join(app.config['UPLOAD_FOLDER'], 'pic2.jpg')
    full_filename3 = os.path.join(app.config['UPLOAD_FOLDER'], 'pic3.jpg')
    full_filename4 = os.path.join(app.config['UPLOAD_FOLDER'], 'pic4.jpg')
    full_filename5 = os.path.join(app.config['UPLOAD_FOLDER'], 'pic5.jpg')
    full_filename6 = os.path.join(app.config['UPLOAD_FOLDER'], 'pic7.jpg')
    return render_template("index.html", user_image = full_filename,user_image2 = full_filename2,user_image3 = full_filename3,user_image4 = full_filename4
                           ,user_image5 = full_filename5,user_image6 = full_filename6)


if __name__ == '__main__':
    app.run()